<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Create Cluster](#create-cluster)
    - [Cert Manager (Optional)](#cert-manager-optional)
    - [Kubernetes Snapshot Automation](#kubernetes-snapshot-automation)
    - [Prometheus](#prometheus)
    - [Adding Container Registry Credentials](#adding-container-registry-credentials)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Create Cluster

There are 4 properties of the cluster you'll want to figure out ahead of time.

* Zone - The zone that you want to place the cluster in and picking the center of the country is better for things that should be collected centrally.
* Name - Name that you want to denote to the cluster for the particular zone it will be in.
* Node Type - Really you should be using memory need for the set of applications to determine whether it should be a n1-standard-1 or something high memory or high cpu.
* Auto Repair - GCP will try to repair nodes on it's own. Which may mean shifting pods on to other machines when pods go down.
* Auto Upgrade - GCP will cordon off machines and replace them with upgraded versions of kubernetes and container images.
* Maintenance Window - The time to start when it's best to perform maintenance operations on the nodes and master. Best to use the regions time window of something like 3 in the morning converted to UTC.
* Node with autoscaling - As additional resources are needed nodes will be added to meet capacity. It may take longer for a pod to stand up, but this makes sure you're using at capacity and not more. Outside of the min and max of course.

```shell
# Create a cluster
gcloud container clusters create CLUSTER_NAME -m n1-standard-1 --enable-autorepair --enable-autoupgrade --maintenance-window=03:13 --enable-autoscaling --min-nodes 2 --max-nodes 8 --disk-size 20 --zone us-central1-b
```

## Cert Manager (Optional)

If you plan on hosting a url that will need an ssl cert then you'll need to set up cert-manager which will retrieve certs from Let's Encrypt. Look in the cert-manager repository for further directions.

## Kubernetes Snapshot Automation

We're using k8s-snapshots to automatically generate snapshot in google cloud for the volumes to allow for restoration in case of a catastrophic event. https://github.com/miracle2k/k8s-snapshots

Look in the [snapshots directory](snapshots/README.me) for further directions.

## Prometheus

If you require metrics output for your cluster, you can deploy the cluster-wide prometheus instance 
like so (from the kubernetes dir):
```shell
cd monitoring
kustomize build . |  kubectl apply -f -
```

## Adding Container Registry Credentials
For the cluster to access external Container Registries, it will need credentials. 
For example, configuring a cluster with access to our Artifactory registry, 
create an Artifactory service account user, and run the following:
```shell
kubectl create secret docker-registry regcred --docker-server=transparentinc-docker-local.jfrog.io --docker-username=<username> --docker-password=<password|api-key> --docker-email=<email-address>
```

