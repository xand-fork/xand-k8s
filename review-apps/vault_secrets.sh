#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts
    The purpose of this script is to retrieve the vault secrets for the review-app environment.
END
)
echo "$HELPTEXT"
}

if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
fi

VAULT_ENVIRONMENT=dev
KUBERNETES_CLUSTER_NAME=development

mkdir -p ./generated
../shared/vault_secrets.sh $VAULT_ENVIRONMENT

pushd ../validator

./vault_scripts/generate_validator_configs_from_vault.sh $VAULT_ENVIRONMENT

# Generate the kustomize yaml for validators and the corresponding deployment files for each validator.
# In dev there are 5 if you're using vault to pull the values.
./generate_kustomize_dev.sh 5 ./generated
popd
