# Snapshots Folder

You can read up more on how the annotations with deltas work by reading [How do the deltas work](https://github.com/miracle2k/k8s-snapshots#how-do-the-deltas-work). The example given is `PT1H P1D P7D` which means every hour there will be a backup of the most recent period that will occur until the 2nd time limit which is a day then the previous 24 hour period will be rolled up into another snapshot and finally those 1 days will be rolled up every 7 days. So you can recover to a previous point in 1 hour increments up until the previous 24 hours. Then you can recover in days until the previous week, and finally you can recover in week increments.

In order to install this you'll need the json creds file for a service account user since that user will be able to actually make the snapshots. 
You can reuse a service account with adequate permissions, or create a new one in the GCP console.
If you reuse a service account in multiple clusters, make sure to create a new key for each cluster.

For example, you can create a new key for the `dev-service-account@xand-dev.iam.gserviceaccount.com` SA with 
```shell
gcloud iam service-accounts keys create dev-service-account.json --iam-account dev-service-account@xand-dev.iam.gserviceaccount.com
```

And then upload it as a secret to your target cluster with:
```shell
kubectl create secret generic gcloud-creds --namespace kube-system --from-file=dev-service-account.json=${Creds Json File}
```
Make sure not to check in the creds json file into source control.

```shell
kubectl apply -f rbac.yaml
kubectl apply -f k8s-snapshots.yaml
```

# Using Snapshots

To use the k8s-snapshots to make sure that your volumes are being automatically backed up.

Add `backup.kubernetes.io/deltas: ${deltas}` with deltas to how often the snapshots should be kept around along with the retentions of those snapshots. A good default is `PT1H P1D P30D P365D`, and Months along with Years have had trouble in the past with k8s-snapshots so sticking with hours and days seems to be a safe bet.

Below is an excerpt of how the zulip persistent volume claim.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: zulip
  annotations:
    backup.kubernetes.io/deltas: PT1H P1D P30D P365D
  creationTimestamp: null
  labels:
    app: zulip
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 40Gi
  storageClassName: standard
```
