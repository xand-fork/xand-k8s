#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts
    The purpose of this script is to retrieve the vault secrets for a given environment and place them
    in the appropriate paths to be picked up by kustomize to create secrets in kubernetes.

    Arguments
        ENVIRONMENT (Required) = The environment name that should be specified for the location in vault of where to look up secrets from.
END
)
echo "$HELPTEXT"
}

if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

ENVIRONMENT=$1

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 1
          ;;
esac

${BASH_SOURCE%/*}/bank_vault_secrets.sh $ENVIRONMENT
${BASH_SOURCE%/*}/wallet_keys_vault_secrets.sh $ENVIRONMENT
