#!/usr/bin/env bash

set -o errexit 	 # abort on nonzero exitstatus
set -o nounset 	 # abort on unbound variable
set -eo pipefail # abort on errors

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts.
    The purpose of this script is to perform the deployment of an environment.

    Arguments
        CHAINSPEC_VERSION (Required) = The chain spec (dockerized) version to use for this deployment
        VALIDATOR_VERSION (Optional) = The SHA of the commit which will be used to pull docker images for this deployment.
        CUSTOM_NAMESPACE (Optional) = The name that could be used to create a namespace.
END
)
echo "$HELPTEXT"
}

# Prints helptext if no arguments were passed in even though they are all
# optional just to allow a user to notice if they did not intend to run the
# script without parameters.
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
fi

CHAINSPEC_VERSION=$1
# Check if arg was passed in
if [[ -z $CHAINSPEC_VERSION ]] ; then
    echo "ERROR: No CHAINSPEC_VERSION passed in as the first arg."
    exit 1
fi

VALIDATOR_VERSION=$2

# Check if the arg was passed in
if [[ -z $VALIDATOR_VERSION ]] ; then
    echo "Warning: The VALIDATOR_VERSION wasn't specified so you might be deploying an old version."
fi

CUSTOM_NAMESPACE=$3

# Get path to repo's /.env file for component versions
SHARED_DIR="$( dirname "${BASH_SOURCE[0]}" )"
REPO_DIR="$( dirname "${SHARED_DIR}" )"
REPO_ABS_DIR="$(realpath ${REPO_DIR})"
ENV_FILE="${REPO_DIR}/.env"
echo "Loading .env file: ${ENV_FILE}"
. $ENV_FILE

if [[ -n $VALIDATOR_VERSION ]] ; then
    # xand-bank-mocks published: https://gitlab.com/TransparentIncDevelopment/product/testing/xand-bank-mocks/-/jobs/504113217
    kustomize edit set image gcr.io/xand-dev/xand-bank-mocks=gcr.io/xand-dev/xand-bank-mocks:${BANK_MOCKS_STABLE_VERSION}
    # prometheus-pipesrv published: https://gitlab.com/TransparentIncDevelopment/product/libs/metrics/-/jobs/501321957
    kustomize edit set image gcr.io/xand-dev/prometheus-pipesrv=gcr.io/xand-dev/prometheus-pipesrv:${PROMETHEUS_STABLE_VERSION}
    kustomize edit set image gcr.io/xand-dev/validator=gcr.io/xand-dev/validator:$VALIDATOR_VERSION
    kustomize edit set image gcr.io/xand-dev/chain-spec=gcr.io/xand-dev/chain-spec:$CHAINSPEC_VERSION
    kustomize edit set image gcr.io/xand-dev/allowlist-updater=gcr.io/xand-dev/allowlist-updater:$VALIDATOR_VERSION
    kustomize edit set image gcr.io/xand-dev/allowlist-nginx=gcr.io/xand-dev/allowlist-nginx:$VALIDATOR_VERSION
    kustomize edit set image gcr.io/xand-dev/trust-api=gcr.io/xand-dev/trust-api:${TRUST_API_STABLE_VERSION}
fi

# This Custom Namespace is expected to be set in review apps case, but is hard set in the other environments.
if [[ -n $CUSTOM_NAMESPACE ]] ; then

    kubectl create namespace --dry-run -o yaml $CUSTOM_NAMESPACE > namespace_temp
    kubectl apply -f namespace_temp
    rm namespace_temp

    if [[ -d namespaced ]] ; then
       cd namespaced
       kustomize edit set namespace $CUSTOM_NAMESPACE
       cd ..
    fi
    NAMESPACE=$CUSTOM_NAMESPACE
else
    kubectl apply -f namespace.yaml
    # Retrieves the namespace from the namespace.yaml file.
    NAMESPACE=$(cat namespace.yaml| grep name: | sed -e "s/.*name: \([^ \n]*\)/\1/")
fi

# Deletes existing validator deployments and persisted volumes to clear out the existing
# chain on every deployment.
if [[ $(kubectl get deployment -n $NAMESPACE -ltier=validator | wc -l) -gt 1 ]] ; then
    echo "Detaching and deleting persisted volumes to start a new deployment with a fresh chain."

    kubectl scale -n $NAMESPACE --replicas 0 -ltier=validator deployment
    kubectl delete pvc -n $NAMESPACE -ltier=validator --wait=true
fi

# Deletes existing trustee deployment and persisted volumes to clear out the existing
# state on every deployment.
if [[ $(kubectl get deployment -n $NAMESPACE -ltier=trustee | wc -l) -gt 1 ]] ; then
    echo "Detaching and deleting persisted volumes to start a new deployment with a fresh chain."

    kubectl scale -n $NAMESPACE --replicas 0 -ltier=trustee deployment
    kubectl delete pvc -n $NAMESPACE -ltier=trustee --wait=true
fi

kustomize build . | kubectl apply -f -

echo "Finished applying configuration to kubernetes waiting for deployments to roll out."
# Wait until the deployment completely rolls out and error out on timeout
# to help devs know sooner that there was an error potentially in the
# configuration of the deployment.
kubectl get deployment -n $NAMESPACE -o name | xargs -n 1 -I {} kubectl rollout status -n $NAMESPACE --timeout 3m --watch=true {}

echo "Finished waiting for deployments to roll out now waiting for ssl certs."

COUNT=0
NO_CERTS=1
# Assuming 10 seconds for each url that means a 70 second
# lapse time between request, but will reduce down once
# certificates are retrieved
while [[ $NO_CERTS -ne 0 && $COUNT -lt 10 ]]
do
    # Wait for the certificates to be available as well.
    # (This might not work as it takes some time for the certs to appear and hoping the previous
    # wait gives enough time for the cert manager to create these objects)
    kubectl wait -n $NAMESPACE --for=condition=Ready certificates --all --timeout=10s && \
            NO_CERTS=$? || NO_CERTS=$?
    COUNT=$(($COUNT+1))

    echo "Iteration $COUNT/10 in waiting for ssl certs."
done

echo "Finished waiting for ssl certs."
