#!/usr/bin/env bash
set -e

# Trim trailing slash if exists
GENERATED_DIR=${1%/}

# Check if arg was passed in
if [[ -z $GENERATED_DIR ]] ; then
    echo "ERROR: The directory for generated artifacts needs to be passed in"
    exit 1
fi

# Check if arg is a directory
if ! [[ -d $GENERATED_DIR ]] ; then
    echo "ERROR: $GENERATED_DIR not found or is not a directory"
    exit 1
fi

ENVIRONMENT=$2

# Check if arg was passed in
if [[ -z $ENVIRONMENT ]] ; then
    echo "ERROR: The environment location in vault needs to be specified."
    exit 2
fi

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 2
          ;;
esac

# Directory for intermediate key files
KEYS_SCRATCH_DIR=${GENERATED_DIR}/keys
mkdir -p ${KEYS_SCRATCH_DIR}

# Load each validator's session keys into the scratch keys dir, subdir v0, v1, ... vn
for i in `seq 0 4`;
do
    VALIDATOR_N_DIR=${KEYS_SCRATCH_DIR}/v${i}
    mkdir -p ${VALIDATOR_N_DIR}

    # Aura Key is for Aura and Session Key is for Grandpa
    FINALIZE_KEY_ADDR=$(vault kv get --field=session-address /secret/environments/$ENVIRONMENT/validator-${i})
    BLOCKPROD_KEY_ADDR=$(vault kv get --field=aura-address /secret/environments/$ENVIRONMENT/validator-${i})

    # These key prefixes must match the prefixes as searched for by the `firstrun` binary
    # and as outputted by xkeygen
    VALIDATOR_N_FILE_FINALIZE="${VALIDATOR_N_DIR}/validator-session-finalize-blocks-${FINALIZE_KEY_ADDR}"
    VALIDATOR_N_FILE_BLOCKPROD="${VALIDATOR_N_DIR}/validator-session-produce-blocks-${BLOCKPROD_KEY_ADDR}"
    FINALIZE_SECRET=$(vault kv get --field=session-private-key /secret/environments/$ENVIRONMENT/validator-${i})
    BLOCKPROD_SECRET=$(vault kv get --field=aura-private-key /secret/environments/$ENVIRONMENT/validator-${i})
    printf "${FINALIZE_SECRET}" > ${VALIDATOR_N_FILE_FINALIZE}
    printf "${BLOCKPROD_SECRET}" > ${VALIDATOR_N_FILE_BLOCKPROD}
    echo "Created session keyfile ${i} for ${FINALIZE_KEY_ADDR} in ${VALIDATOR_N_DIR}"
    echo "Created aura keyfile ${i} for ${BLOCKPROD_KEY_ADDR} in ${VALIDATOR_N_DIR}"
done
