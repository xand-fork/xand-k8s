#!/usr/bin/env bash

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation or humans. Given a configuration file (specifying
    identities, allowlists, block config) and a path to a chain spec .json file, and path to output file,
    merge the local configurations and write the resulting chain spec to the target output file.

    Arguments:
        CHAINSPEC_GENERATOR_CONFIG (Required) = YAML file specifying initial on-chain identities, for chain-spec-generator
        DOCKER_REGISTRY (Required) = Docker registry to pull/push from. One of ["gcr.io/xand-dev", "transparentinc-docker-external.jfrog.io"]
        VALIDATOR_DOCKER_IMAGE_VERSION_ARG (Required) = Version of the docker image
        OUTPUT_DIR_ARG (Required) = Path to write chainspec intermediate files out to.
        TAG_PREFIX_ARG (Optional) = Optionally prefix the docker image tag. Default tag is the current timestamp (UTC)
        TAG_OVERRIDE (Optional) = Override default tag

    Expected Environment Variables:
        ARTIFACTORY_USER (Required) = Artifactory Username to use to upload the zip.
        ARTIFACTORY_PASS (Required) = Artifactory Password to use to upload the zip.
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

# Verify args are set
CHAINSPEC_GENERATOR_CONFIG=${1:?"$( error 'CHAINSPEC_GENERATOR_CONFIG must be set' )"}
DOCKER_REGISTRY=${2:?"$( error 'DOCKER_REGISTRY must be set' )"}
VALIDATOR_DOCKER_IMAGE_VERSION_ARG=${3:?"$( error 'VALIDATOR_DOCKER_IMAGE_VERSION must be set' )"}
OUTPUT_DIR_ARG=${4:?"$( error 'OUTPUT_DIR must be set' )"}
TAG_PREFIX_ARG=${5:-""}
CHAIN_SPEC_IMAGE_TAG_SUFFIX=${6:-"$(date --utc +%Y%m%d_%H%M%SZ)"}

ARTIFACTORY_USER=${ARTIFACTORY_USER:?"$( error 'ARTIFACTORY_USER must be set')"}
ARTIFACTORY_PASS=${ARTIFACTORY_PASS:?"$( error 'ARTIFACTORY_PASS must be set')"}

CHAINSPEC_GENERATOR_IMAGE="gcr.io/xand-dev/chain-spec-generator:13.0.0"

mkdir -p $OUTPUT_DIR_ARG

# Download and unzip chain spec template
CHAIN_SPEC_TEMPLATE_URL="https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/chain-spec-template.${VALIDATOR_DOCKER_IMAGE_VERSION_ARG}.zip"
wget --user="$ARTIFACTORY_USER"  --password="$ARTIFACTORY_PASS" -O "$OUTPUT_DIR_ARG/chainSpecTemplate.zip" "$CHAIN_SPEC_TEMPLATE_URL"

# Run script to build the modified, human-readable chain spec
docker run --rm \
    -v $(realpath $CHAINSPEC_GENERATOR_CONFIG):/config.yaml \
    -v $(realpath $OUTPUT_DIR_ARG/chainSpecTemplate.zip):/chainspec_template.zip \
    -v $(pwd)/$OUTPUT_DIR_ARG:/output \
    $CHAINSPEC_GENERATOR_IMAGE \
    generate \
        --chainspec-zip /chainspec_template.zip \
        --config /config.yaml \
        --output-dir /output

# Use validator binary to create "raw" version of chain spec
docker pull ${DOCKER_REGISTRY}/validator:$VALIDATOR_DOCKER_IMAGE_VERSION_ARG
docker run --rm -v $(realpath $OUTPUT_DIR_ARG/chainSpecModified.json):/xand/chainSpecModified.json --entrypoint ./xandstrate ${DOCKER_REGISTRY}/validator:$VALIDATOR_DOCKER_IMAGE_VERSION_ARG build-spec --raw --chain /xand/chainSpecModified.json > $OUTPUT_DIR_ARG/chainSpecRaw.json

pushd $OUTPUT_DIR_ARG

# Build docker image

CHAIN_SPEC_IMAGE_TAG=$CHAIN_SPEC_IMAGE_TAG_SUFFIX

# If prefix is specified then tag becomes "<PREFIX>-<timestamp>"
if [[ -n $TAG_PREFIX_ARG ]] ; then
    CHAIN_SPEC_IMAGE_TAG="$TAG_PREFIX_ARG-$CHAIN_SPEC_IMAGE_TAG"
fi

CHAIN_SPEC_IMAGE=${DOCKER_REGISTRY}/chain-spec:${CHAIN_SPEC_IMAGE_TAG}
docker build -t $CHAIN_SPEC_IMAGE --file ../Dockerfile .
popd

echo
echo
echo "Please spot check contents of these files if necessary."
echo "Filled Chainspec is located at $OUTPUT_DIR_ARG/chainSpecModified.json"
echo "Raw Chainspec is located at $OUTPUT_DIR_ARG/chainSpecRaw.json"

read -p "Do you want to publish these contents? (y/n) " -n 1 -r

if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
    exit 1
fi

# Push docker image
docker push $CHAIN_SPEC_IMAGE

echo
echo
echo "Published image to $CHAIN_SPEC_IMAGE"
echo
echo
echo "Ansible Variables:"
echo "  chain_spec_version: \"$CHAIN_SPEC_IMAGE_TAG\""
