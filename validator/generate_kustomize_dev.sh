#!/usr/bin/env bash
set -e

NUMBER_RE='^[0-9]+$'

NUMBER_OF_VALIDATORS=$1

if ! [[ $NUMBER_OF_VALIDATORS =~ $NUMBER_RE ]] ; then
    echo "ERROR: The nuber of validators to generate needs to be passed in arg 1."
    exit 1
fi

# Trim trailing slash if exists
GENERATED_DIR=${2%/}

# Check if arg was passed in
if [[ -z $GENERATED_DIR ]] ; then
    echo "ERROR: The directory for generated artifacts needs to be passed in arg 2"
    exit 1
fi

# Check if arg is a directory
if ! [[ -d $GENERATED_DIR ]] ; then
    echo "ERROR: $GENERATED_DIR not found or is not a directory"
    exit 1
fi

rm -f kustomization.yaml
touch kustomization.yaml

GENERATED_VALIDATOR_ARTIFACTS_DIR=${GENERATED_DIR}/validatork8s

declare -a VALIDATOR_FILE_TYPES=("deployment.yaml" "pvc.yaml" "service.yaml")

for i in `seq 0 $(($NUMBER_OF_VALIDATORS-1))`
do
    kustomize edit add secret validator-$i-lp2p-key --from-env-file ${GENERATED_VALIDATOR_ARTIFACTS_DIR}/validator-${i}-lp2p-key.env
    kustomize edit add configmap validator-$i-config --from-env-file ${GENERATED_VALIDATOR_ARTIFACTS_DIR}/validator-${i}-config.env

    # Loops through the file types for a single validator then transforms them and outputs them with an index.
    # e.g `validator.deployment.yaml.template` becomes `validator-0.deployment.yaml`.
    for validator_file_type in "${VALIDATOR_FILE_TYPES[@]}"
    do
        FILE_PATH=${GENERATED_VALIDATOR_ARTIFACTS_DIR}/validator-${i}.$validator_file_type
        cat ./validator.$validator_file_type.template | sed -e "s|\${VALIDATOR_NAME}|validator-$i|g" > $FILE_PATH

        kustomize edit add resource $FILE_PATH
        echo "Created and added validator-$i.$validator_file_type file to kustomize."
    done
done

# Add all session keys as secrets
SCRATCH_KEYS_DIR=${GENERATED_DIR}/keys
for i in `seq 0 $(($NUMBER_OF_VALIDATORS-1))`
do
    for keyfile in ${SCRATCH_KEYS_DIR}/v${i}/*
    do
        SECRET_NAME=sessionkey-validator-${i}
        kustomize edit add secret ${SECRET_NAME} --from-file=${keyfile}
        echo "Added $keyfile as secret ${SECRET_NAME}"
    done
done

# Add network wide resources
kustomize edit add resource validator-telemetry.yaml
echo "Added validator-telemetry.yaml file to kustomize."

kustomize edit add resource validator-network.service.yaml
echo "Added validator-network.ingress.yaml file to kustomize."

kustomize edit add resource allowlister.service.yaml
echo "Added allowlister.service.yaml file to kustomize."

kustomize edit add resource allowlister.deployment.yaml
echo "Added allowlister.deployment.yaml file to kustomize."

kustomize edit add resource allowlist-updater.configmap.yaml
echo "Added allowlist-updater.configmap.yaml to kustomize."

cat <<EOF >> kustomization.yaml
generatorOptions:
  disableNameSuffixHash: true
EOF
