#!/usr/bin/env bash
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to be able to automate the creation of kustomization.yaml file and associated files
    for our partners including non-consensus and consensus nodes.

    Arguments
        --consensus (Optional) = If set will output the consensus version of a node.
        OUTPUT_DIR (Required) = The directory where the resulting files will be output to.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

OPTIONS=$(getopt -o hc --long help,consensus -- "$@")
if [ $? -ne 0 ] ; then
    echo "Incorrect options."
    echo
    echo "$(helptext)"
fi
eval set -- $OPTIONS

CONSENSUS_NODE=FALSE
while true; do
    case "$1" in
        -h|--help)
            echo "$(helptext)"
            exit 1
            ;;
        -c|--consensus)
            CONSENSUS_NODE=TRUE
            ;;
        --)
            shift
            break
            ;;
        *)
            break
            ;;
    esac
    shift
done

# Trim trailing slash if exists
OUTPUT_DIR=${1%/}

# Check if arg was passed in
if [[ -z $OUTPUT_DIR ]] ; then
    echo "ERROR: The directory for generated artifacts needs to be passed in first arg."
    exit 1
fi

# Check if arg is a directory
if ! [[ -d $OUTPUT_DIR ]] ; then
    echo "ERROR: $OUTPUT_DIR not found or is not a directory"
    exit 1
fi

echo "--- Starting on creating the right deployment files for a partner ---"

echo "Outputting results in to the following directory: ${OUTPUT_DIR}"
cp ${BASH_SOURCE%/*}/kustomization_partner.yaml $OUTPUT_DIR/kustomization.yaml
cp ${BASH_SOURCE%/*}/validator-network.service.yaml $OUTPUT_DIR/

declare -a VALIDATOR_FILE_TYPES=("deployment.yaml" "pvc.yaml" "service.yaml" "ingress.yaml")

# Loops through the file types for a single validator then transforms them and outputs them with an index.
# e.g `validator.deployment.yaml.template` becomes `validator.deployment.yaml`.
for validator_file_type in "${VALIDATOR_FILE_TYPES[@]}"
do
    FILE_PATH=${OUTPUT_DIR}/validator.$validator_file_type
    cat ${BASH_SOURCE%/*}/validator.$validator_file_type.template | sed -e "s|\${VALIDATOR_NAME}|validator|g" > $FILE_PATH

    pushd ${OUTPUT_DIR}
    kustomize edit add resource $(basename $FILE_PATH)
    popd
    echo "Added resource $FILE_PATH to ${OUTPUT_DIR}/kustomization.yaml"
done

# The following will perform some transformations for consensus/non-consensus and general partner transformations.
# Start by creating a temporary directory to have a kustomization file at the top level of that directory
TEMP_DIR=$(mktemp -d)

# Copy over the newly created validator deployment file and copy over all artifacts into the
# folder since the kustomization may use those files to transform the deployment.
cp ${OUTPUT_DIR}/validator.deployment.yaml ${TEMP_DIR}/
cp ${BASH_SOURCE%/*}/partner_artifacts/* ${TEMP_DIR}/

# Set which kustomization file to use based on the node type.
if [[ "$CONSENSUS_NODE" = "TRUE" ]] ; then
    mv ${TEMP_DIR}/kustomization_consensus.yaml ${TEMP_DIR}/kustomization.yaml
else
    mv ${TEMP_DIR}/kustomization_non_consensus.yaml ${TEMP_DIR}/kustomization.yaml
fi

# Build the new deployment file and output it to the output directory.
NEW_DEPLOYMENT=$(kustomize build ${TEMP_DIR})
echo "$NEW_DEPLOYMENT" > ${OUTPUT_DIR}/validator.deployment.yaml
rm -rf ${TEMP_DIR}

echo "Updated Deployment File:"
echo "$NEW_DEPLOYMENT"
echo "The above result will be in the $OUTPUT_DIR."

echo "--- Finished creating the right deployment files for a partner ---"

echo "--- Starting to create the config file templates and kustomize for a partner ---"

function get_kustomize_file_arg {
    local FILENAME=$1
    if [[ $FILENAME =~ \.env$ ]] || [[ $FILENAME =~ \.properties$ ]] ; then
        echo "--from-env-file $(basename ${FILENAME})"
    else
        echo "--from-file $(basename ${FILENAME})"
    fi
}

# This will copy the files from the partner config directory (Template files for our configs) to the output directory
# while adding them to the kustomize based on the name that will be created in k8s.
#
# Arg 1 (K8S_CONFIG_TYPE) is either "configmap|secret" based on the type that will be added to kustomize.
# Arg 2 (ASSOCIATIVE_ARRAY_FILES) is an associative array where the key is the file path and the value is the name
# of the secret in kustomize. It's reversed since otherwise a k8s name can have multiple files. So the key being the
# file path creates uniqueness in the key.
#
# More on usage of associative arrays: https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/
# Passing associative arrays is funky too: https://stackoverflow.com/a/8879444/1807040
function copy_and_add_to_kustomize {
    local K8S_CONFIG_TYPE=$1
    eval "declare -A ASSOCIATIVE_ARRAY_FILES="${2#*=}

    for validator_config_file in "${!ASSOCIATIVE_ARRAY_FILES[@]}"
    do
        cp ${BASH_SOURCE%/*}/partner_config/$validator_config_file ${OUTPUT_DIR}/
        K8S_CONFIG_NAME=${ASSOCIATIVE_ARRAY_FILES[$validator_config_file]}

        pushd ${OUTPUT_DIR}
        eval "kustomize edit add $K8S_CONFIG_TYPE $K8S_CONFIG_NAME $(get_kustomize_file_arg $validator_config_file)"
        popd
        echo "Added $K8S_CONFIG_TYPE $validator_config_file to ${OUTPUT_DIR}/kustomization.yaml"
    done
}


declare -A CONFIGMAP_FILES=( ["validator-config.env"]="validator-config" )
copy_and_add_to_kustomize configmap "$(declare -p CONFIGMAP_FILES)"

declare -A SECRET_FILES=( [jwt_secret]="jwt-secret" )

if [[ "$CONSENSUS_NODE" = "TRUE" ]] ; then
    SECRET_FILES["consensus/validator-lp2p-key.env"]="validator-lp2p-key"
    SECRET_FILES["consensus/validator-session-finalize-blocks"]="sessionkey-validator"
    SECRET_FILES["consensus/validator-session-produce-blocks"]="sessionkey-validator"
fi

# This will only include the external load balancer and allowlister service for consensus nodes
if [[ "$CONSENSUS_NODE" = "TRUE" ]] ; then
    # Include allowlister service in output.
    cp ${BASH_SOURCE%/*}/allowlist-updater.configmap.yaml $OUTPUT_DIR/
    cp ${BASH_SOURCE%/*}/allowlister.deployment.yaml $OUTPUT_DIR/
    cp ${BASH_SOURCE%/*}/allowlister.service.yaml $OUTPUT_DIR/

    # Include allowlister service in final kustomize resources.
    pushd ${OUTPUT_DIR}
    kustomize edit add resource allowlist-updater.configmap.yaml
    kustomize edit add resource allowlister.deployment.yaml
    kustomize edit add resource allowlister.service.yaml
    popd
fi

copy_and_add_to_kustomize secret "$(declare -p SECRET_FILES)"

echo "--- Finished the creation of the config file templates and kustomize for a partner ---"

# For gitlab job that picks up version from location where it's zipped.
cp ${BASH_SOURCE%/*}/version.txt ${OUTPUT_DIR}/
