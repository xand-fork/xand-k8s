#!/usr/bin/env bash

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation. Given a namespace and image versions,
    deploy the Trust node to the connect k8s cluster
	K8S_NAMESPACE (Required) = Namespace
	TRUST_IMAGE_TAG (Required) = Trustee docker image version. Assumes image: gcr.io/xand-dev/trust
	PROMETHEUS_IMAGE_TAG (Required) = Prometheus pipe serve docker image version. Assumes image: gcr.io/xand-dev/prometheus-pipesrv
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

# Verify args are set
K8S_NAMESPACE=${1:?"$( error 'K8S_NAMESPACE must be set' )"}
TRUST_IMAGE_TAG=${2:?"$( error 'TRUST_IMAGE_TAG must be set' )"}
PROMETHEUS_IMAGE_TAG=${3:?"$( error 'PROMETHEUS_IMAGE_TAG must be set' )"}

# Get path to /trustee
TRUSTEE_DIR="$( dirname "${BASH_SOURCE[0]}" )"
TRUSTEE_DIR_ABS_PATH="$(realpath ${TRUSTEE_DIR})"

# cwd to /trustee
pushd $TRUSTEE_DIR

# Set image versions
TRUST_IMAGE="gcr.io/xand-dev/trust:$TRUST_IMAGE_TAG"
PROMETHEUS_PIPE_SRV_IMAGE="gcr.io/xand-dev/prometheus-pipesrv:$PROMETHEUS_IMAGE_TAG"

echo "Setting trust image to $TRUST_IMAGE"
kustomize edit set image trust=$TRUST_IMAGE

echo "Setting prometheus pipe srv image to $PROMETHEUS_PIPE_SRV_IMAGE"
kustomize edit set image prometheus-pipesrv=$PROMETHEUS_PIPE_SRV_IMAGE

# Set namespace for resources
echo "Setting namespace to $K8S_NAMESPACE"
kustomize edit set namespace $K8S_NAMESPACE

# Build application.yaml file
echo "Running kustomize build after editing"
kustomize build > application.yaml

# Deploy (kubectl apply) result
echo "Running kustomize apply -f application.yaml"
kubectl apply -f application.yaml

popd
