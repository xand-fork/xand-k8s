# Developer Setup

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Installing and configuring Kubernetes, Google Cloud SDK, and Service Mesh](#installing-and-configuring-kubernetes-google-cloud-sdk-and-service-mesh)
    - [Kubernetes Dashboard](#kubernetes-dashboard)
- [Install Deployment Dependencies](#install-deployment-dependencies)
    - [Kustomize](#kustomize)
    - [JQ](#jq)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installing and configuring Kubernetes, Google Cloud SDK, and Service Mesh
First, you'll want to install the Google Cloud (GCP) SDK from [here](https://cloud.google.com/sdk/). 

First, you'll want to install the Google Cloud SDK from [here](https://cloud.google.com/sdk/). Follow the steps including running the `gcloud init` step that will authenticate your client with the Google Cloud authentication service. After you are authenticated, make sure you have access to the "Transparent Development" Google Cloud project. When you type in `gcloud projects list` you should see it in the list like so:

```shell
gcloud projects list
PROJECT_ID              NAME                     PROJECT_NUMBER
xand-dev                Transparent Development  #############
```

*If you do not see the xand-dev project in the projects list, reach out to a fellow engineer.* 

After that you'll most likely want to update kubernetes command line tool, `kubectl`.

```shell
# updates command line tools for kubernetes web service
gcloud components update kubectl
```

Then configure your GCP project:

```shell
# Set the project to work within
gcloud config set project xand-dev

# Set the google compute zone
gcloud config set compute/zone us-central1-b
```

List all the clusters:
```shell script
gcloud container clusters list
```

If you want to subscribe to a cluster 
```shell script
# Set your default cluster
# gcloud config set container/cluster CLUSTER
gcloud config set container/cluster tools
```

To access the cluster via `kubectl` merge the cluster's config with 
```shell
# gcloud container clusters get-credentials <cluster-name>
gcloud container clusters get-credentials tools
```

Currently, the dev GCP cluster is named "tools".

### Kubernetes Dashboard
The kubernetes UI/dashboard has been installed on the `tools` cluster. ([Instructions](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) for reference)

To access the dashboard, first fetch the token
```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

Then open an SSH tunnel with
```
kubectl proxy
```
And navigate to: 
`http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/`

>If your namespace has not yet been defined, navigate to:
`http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=default`

Copy and paste the token to login. You can have your browser remember the token, as this token does not change over time.


## Install Deployment Dependencies

### Kustomize

```shell
curl -O -L https://github.com/kubernetes-sigs/kustomize/releases/download/v3.1.0/kustomize_3.1.0_linux_amd64 && \
  sudo mv kustomize_*_linux_amd64 /usr/bin/kustomize && \
  sudo chmod u+x /usr/bin/kustomize
```
### JQ

```shell
sudo apt install jq
```

If jq isn't available on your package manager:

```shell
# Install JQ based on https://github.com/stedolan/jq/wiki/Installation#with-docker
ENV JQ_VERSION='1.5'

RUN curl -s -L https://raw.githubusercontent.com/stedolan/jq/master/sig/jq-release.key -o /tmp/jq-release.key && \
    curl -s -L https://raw.githubusercontent.com/stedolan/jq/master/sig/v${JQ_VERSION}/jq-linux64.asc -o /tmp/jq-linux64.asc && \
    curl -s -L https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -o /tmp/jq-linux64 && \
    gpg --import /tmp/jq-release.key && \
    gpg --verify /tmp/jq-linux64.asc /tmp/jq-linux64 && \
    cp /tmp/jq-linux64 /usr/bin/jq && \
    chmod +x /usr/bin/jq && \
    rm -f /tmp/jq-release.key && \
    rm -f /tmp/jq-linux64.asc && \
    rm -f /tmp/jq-linux64
```
