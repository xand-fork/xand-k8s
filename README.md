# xand-k8s

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Working in this repo](#working-in-this-repo)
  - [The `.env` file](#the-env-file)
  - [`xand-k8s` Review Apps](#xand-k8s-review-apps)
  - [Breaking Changes](#breaking-changes)
  - [Triggering Deploy + Verify from upstream repos](#triggering-deploy--verify-from-upstream-repos)
- [Deploying a review app from local scripts](#deploying-a-review-app-from-local-scripts)
- [Clearing out validator state of a long running environment.](#clearing-out-validator-state-of-a-long-running-environment)
- [Accessing ingress points without DNS](#accessing-ingress-points-without-dns)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This project is licensed under MIT OR Apache-2.0

This repo houses k8s definitions for services and components that can come together to run different pieces
of the Xand Network.

This repo's CI is responsible for publishing versioned packages per service or component, such that
they are independently consumable by 3rd-party processes and partners. Often, a consumer would want
to compose different packages together to run a "production" grade application on top of a Kubernetes cluster.

For example, a Trustee instance might run on a cluster alongside _Metrics_ (Prometheus) and _Logging Exporter_ (loggly)
services.

This repo's CI additionally packages together such subsets of services, versioning and publishing them
for downstream processes to consume.

## Working in this repo

### Branch names
Branch names must be 64 characters or less to pass CI.

### The `.env` file
The `/.env` file in this repo currently represents a version set of (xand) components that have been
successfully deployed to a development K8s cluster together, passing Acceptance Tests.

### Dev Network Configuration
The deployed development K8s cluster consists of the following components:
- member-0 (AwesomeCo)
- member-1 (CoolInc)
- trustee
- validator-0 (configured to be member-0's node)
- validator-1 (configured to be member-1's node)
- validator-2 (configured to be the trustee's node)
- validator-3 (configured with all entities' keys)
- validator-4 (configured with all entities' keys)

### `xand-k8s` Review Apps

To make changes to the K8s resource definitions, open and push a branch in the repo. Every open branch
in this repo deploys an associated environment, registered as a
[GitLab "review-app"](https://gitlab.com/TransparentIncDevelopment/product/devops/xand-k8s/-/environments), using
the versions specified in the `/.env` file.

We use the following naming convention for namespacing deployments in the `development` cluster:
```
<$CI_PROJECT_ID>-<$CI_COMMIT_REF_SLUG>
```

So the namespace for the master branch in the `xand-k8s` repo would be: `17995502-master`.

### Breaking Changes

If there are deployment-breaking changes to components, go ahead and merge the upstream component, and verify
it passes its own integration tests. Once a stable version is published, open 1 MR in this repo, carrying
the version bump in the `/.env` file with any deployment changes required.

If you _must_ test component changes and K8s deployment changes before merging in the component changes to its
repo, you can do so by setting the upstream repo to trigger a branch here holding your deployment changes.
1. Open a branch in this repo with your deployment changes.
1.
    In the upstream repo's review app (more info below), add
    ```
    export REF_BRANCH="<your_xandk8s_branch>"
    ```
    to the `before_script` section. This will trigger a pipeline off of that branch instead of `master`.

### Triggering Deploy + Verify from upstream repos

This repo exports job definitions that upstream repos can `include`. These are utility job definitions for
upstream to define their own review-apps, and trigger the deploy + verify steps in this repo.

For example, a review-app kicked off from `thermite`, on a branch called `feature-x` would be deployed
to the namespace `9441604-feature-x`, and the review-app would be found in the [`thermite` repo](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/environments).

When upstream repos trigger this repo to kick off a deployment, they can set any of the `*VERSION_ARG`
variables in the `before_script: ` section of the job to deploy a specific version of a component.

[`gitlab-ci/exported/review-apps`](/gitlab-ci/exported/review-apps)
```
variables:
    # Optional vars; Can only be overridden in `before_script` section
    VALIDATOR_VERSION_ARG: ""
    MEMBER_API_VERSION_ARG: ""
    TRUSTEE_VERSION_ARG: ""
    BANK_MOCKS_VERSION_ARG: ""
```

For example, `thermite` review_apps can do the following to use the versions
produced on the respective branch by setting
```
before_script:
    - export VALIDATOR_VERSION_ARG=$CI_COMMIT_SHA
    - export TRUSTEE_VERSION_ARG=$CI_COMMIT_SHA
```

## Deploying a review app from local scripts

In order to deploy a review app to a kubernetes cluster you will need to
[install kustomize](https://github.com/kubernetes-sigs/kustomize/blob/master/docs/INSTALL.md).

The default means to deploy the application would be based on an existing docker image that has been published which you can retrieve via the latest commit sha from the `origin/develop` branch.

```shell
export CI_COMMIT_SHA=$(git rev-parse --verify origin/develop)
```

Otherwise you can publish images by specifying the CI_COMMIT_SHA and then running cargo make to publish docker images of what the
last commit from your local machine. (This will require authentication to google cloud to publish to the container registry)

```shell
export CI_COMMIT_SHA=$(git rev-parse --verify HEAD)
cargo make --env-file=.ci/.env -p production docker-build-publish-flow
```

The steps below will deploy using the commit sha specified above and then you can pick a name space and a base url.

```shell
# Choose the development cluster to start with.
gcloud container clusters get-credentials development

cd review-apps
./deploy.sh $COMMIT_SHA ${A-Custom-Namespace-That-You-Pick} ${A-Url-Friendly-SLUG-That-You-Pick}
```


## Clearing out validator state of a long running environment.

Previously we would delete the namespace to clear everything out however this will also delete out the certificates for a given namespace which will cause a quota max out from Let's Encrypt which may cause us to wait a week to get new certificates.

If you would like to clear the state of the existing validators before a deployment. You can follow these steps to remove the existing hard disks for a given validator. The steps below using the `kubectl edit` expect that your `EDITOR` environment variable is set to most likely either `vim` or `emacs`. Otherwise you can use the google cloud console to perform the same edits. Start at the [workloads screen](https://console.cloud.google.com/kubernetes/workload?project=xand-dev) and then filter by namespace and the name validtor. Then choose one of the deployments and select edit.

First we will edit the validator deployments to have replicas of 0 so you can use the UI above to perform the edits or use the following CLI tools to perform the edits.

```shell
# Choose the development cluster to start with.
gcloud container clusters get-credentials development
```

```shell
kubectl edit deployment -n dev validator-0
kubectl edit deployment -n dev validator-1
kubectl edit deployment -n dev validator-2
kubectl edit deployment -n dev validator-3
kubectl edit deployment -n dev validator-4
```

After performing the edits above the validators should remove the existing pods that relate to those validators. So from here we can start performing deletions on the persistent volume claims (pvc) these tie the pod to a persistent volume. When the volume is released they will automatically delete.

```shell
seq 0 4 | xargs -n 1 -I {} kubectl delete pvc --namespace dev validator-{}
```

From here you may want to check the status of the deletion by rerunning the following command until ther persistent volumes (pv) disappear for the validator and namespace that you are looking for.

```shell
kubectl get pv --namespace dev
```

Subsequent deployments should repopulate the persistent volume claims and cause a new disk to be created. Follow the steps below to do it manually.


## Accessing ingress points without DNS

If you are testing your own cluster, you probably don't have DNS setup, and you
have no real reason to want it.

To test that ingresses are functioning properly wihout DNS, you can use cURL
to specify the "host" in the header.

```shell
curl --resolve prometheus-server.dev.xand.tools:443:52.149.51.111 -k -L -H "Host: prometheus-server.dev.xand.tools"
```

Just replace the IP with the publicly exposed IP for the nginx ingress controller
from your cloud service.

This is currently (hopefully, temporarily) necessary because the nginx controller
is routing based on who the client says the host is and no other mechanism. We
should also add a normal routing based mechanism.

## Updating validator

The version in `./validator/version.txt` must be bumped when the validator is updated or publishing will fail.
