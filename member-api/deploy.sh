#!/usr/bin/env bash

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -eo pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation. It unzips the given zipfile, and copies
    over configuration, and making last mile modifications before deploying for an internal dev network.

	MEMBER_NODE_IDX (Required) = the id for the member api node (0 or 1)
	MEMBER_NAME (Required) = the member associated with the instance of the member api (awesomeco or coolinc)
	ENVIRONMENT (Required) = the environment to load identities from
	ZIP_FILENAME (Required) = Zip filename with member-api-k8s resources
	IMAGE (Required) = Fully docker image to use for member-api. Ex. 'gcr.io/xand-dev/member-api'
	VERSION (Required) = Docker image version (e.g. '1.4.5')
	K8S_NAMESPACE (Required) = Target K8s namespace
  MEMBER_API_DNS = Member-api domain name that will host the service (e.g. 'member-api.blah.reviewapp.xand.tools')
"
echo "$HELPTEXT"
}

function error {
  echo "$@"
  echo
  echo "$(helptext)"
  exit 1
}

# No Arguments
if [[ $# -eq 0 ]] ; then
  error "Missing arguments"
fi

# Verify args are set

MEMBER_NODE_IDX=${1:?"$( error 'MEMBER_NODE_IDX must be set' )"}
MEMBER_NAME=${2:?"$( error 'MEMBER_ADDRESS must be set' )"}
ENVIRONMENT=${3:?"$( error 'ENVIRONMENT must be set' )"}
ZIP_FILENAME=${4:?"$( error 'ZIP_FILENAME must be set' )"}
IMAGE=${5:?"$( error 'IMAGE must be set' )"}
VERSION=${6:?"$( error 'VERSION must be set' )"}
K8S_NAMESPACE=${7:?"$( error 'K8S_NAMESPACE must be set' )"}
MEMBER_API_DNS=${8:?"$( error 'MEMBER_API_DNS must be set' )"}

OUTPUT_FILE=".output/${K8S_NAMESPACE}.yaml"

echo "Loading member address from vault"
MEMBER_ADDRESS=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/authorized-participants/$MEMBER_NAME)

echo "Unzipping ${ZIP_FILENAME} ..."

# clear any pre-existing workspace
rm -rf ./workspace
# unzip into the workspace
unzip -o ${ZIP_FILENAME} -d ./workspace

echo "Overwriting template configs"
cp -r ./app/. ./workspace/overlays/octopus
cp -r ./app/. ./workspace/base

# Enter workspace
pushd workspace

echo "kustomize location:"
which kustomize

echo "Editing kustomization suffix"
sed -i "s/nameSuffix: \"-0\"/nameSuffix: \"-${MEMBER_NODE_IDX}\"/g" ./overlays/octopus/kustomization.yaml

# TODO: this hack should be addressed in https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5897
echo "Editing kustomization label"
sed -i "s/app: member-api/app: member-api-${MEMBER_NODE_IDX}/g" ./overlays/octopus/kustomization.yaml

echo "Building k8s artifacts for ${K8S_NAMESPACE}"
mkdir -p `dirname ${OUTPUT_FILE}`
./configure-kubernetes.sh -a ${MEMBER_ADDRESS} -d ${MEMBER_API_DNS} -v ${VERSION} -n ${K8S_NAMESPACE} -f ${OUTPUT_FILE} -i ${IMAGE} -l overlays/octopus

echo "Editing xand api url"
sed -i "s/#{member.xand-api-url}/http:\/\/validator-${MEMBER_NODE_IDX}:10044/g" ${OUTPUT_FILE}

echo "Output file:"
cat ${OUTPUT_FILE}

echo "Deploying to ${K8S_NAMESPACE}"
kubectl apply -f ${OUTPUT_FILE}

popd
